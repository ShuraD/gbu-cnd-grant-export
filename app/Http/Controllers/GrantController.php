<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Report;
use \Exception;

class GrantController extends Controller
{
    const EXPORT_FILENAME = 'export.xlsx';

    public function export(Request $request, Report $report)
    {
        try {
            $report->build($request->all(), self::EXPORT_FILENAME);
        } catch (Exception $e) {
            // TODO: log exception?
            return response($e->getMessage(), 500);
        }
        return response()->download(base_path('app') . DIRECTORY_SEPARATOR . self::EXPORT_FILENAME);
    }
}
