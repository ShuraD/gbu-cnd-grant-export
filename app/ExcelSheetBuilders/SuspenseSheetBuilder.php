<?php

namespace App\ExcelSheetBuilders;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SuspenseSheetBuilder
{
    public function build(Worksheet $worksheet, $chunk, $object)
    {
        $worksheet->setCellValue('C4', $object['cadnum']); // Кадастровый номер участка
        $worksheet->setCellValue('C5', null); // Номер строки в таблице 2 листа "Объект права", в которой указан объект
        $worksheet->setCellValue('C6', null); // Номер строки в таблице 3 листа "Объект права", в которой указан объект
        $worksheet->setCellValue('C7', $object['type']); // Вид объекта
        $worksheet->setCellValue('C8', $object['name']); // Наименование объекта
        $worksheet->setCellValue('C9', $chunk['suspensionDate']); // Дата приостановки
        $worksheet->setCellValue('C10', $chunk['suspensionEnd']); // Окончание приостановки
        $worksheet->setCellValue('C11', '???'); // Окончание приостановки
        // Объект общественного питания
        // Является одним из следующих объектов (выбрать один из вариантов):
//        $worksheet->setCellValue('C18', $chunk['suspensionEnd']);

        $worksheet->setCellValue('C25', $chunk['servicedByTakeaway']); // Объект не обслуживал на вынос
        $worksheet->setCellValue('C26', $chunk['workedDeliveryOrders']); // Объект не работал на доставку заказов
        $worksheet->setCellValue('C27', $chunk['cateringEmployeeOrganization']); // Объект не является столовой, буфетом, кафе и иным предприятием питания, осуществляющим организацию питания для работников организаций

        $worksheet->setCellValue('C29', $chunk['retailEntity']); // Является объектом розничной торговли
        // Не является одним из следующих объектов:
        $worksheet->setCellValue('C31', $chunk['sfNOTpharmacy']); // Не является аптекой
        $worksheet->setCellValue('C32', $chunk['sfNOTpharmacyPoint']); // Не является аптечным пунктом
        $worksheet->setCellValue('C33', $chunk['sfNOTcommunicationServicesProvision']); // Не является объектом розничной торговли, в котором осуществляется заключение договоров на оказание услуг связи и реализация связанных с данными услугами средств связи (в том числе мобильных телефонов, планшетов)
        $worksheet->setCellValue('C34', $chunk['sfNOToptophthalmologicalProvision']); // Не является специализированным объектом розничной торговли, реализующим медицинские и оптикоофтальмологические изделия (оборудование)
        $worksheet->setCellValue('C35', $chunk['sfNOTpetSupplesProvision']); // Не является специализированным объектов розничной торговли, реализующим зоотовары
        $worksheet->setCellValue('C36', $chunk['sfNOTfoodProvision']); // Не является объектом розничной торговли в части реализации продовольственных товаров
        $worksheet->setCellValue('C37', $chunk['sfNOTfirstFoodsProvision']); // Не является объектом розничной торговли в части реализации непродовольственных товаров первой необходимости, указанных в приложении 1 к Указу Мэра Москвы № 12-УМ*
        $worksheet->setCellValue('C38', $chunk['shoppingFacilityRemoteSaleGoods']); // Не осуществлял продажу товаров дистанционным способом, в том числе с условием доставки
        // Объект бытовых услуг
//        $worksheet->setCellValue('C40', $chunk['shoppingFacilityRemoteSaleGoods']); // Является одним из следующих объектов (выбрать один из вариантов):

        $worksheet->setCellValue('C49', '???'); // описание "иного объекта" (если выбрано предыдущее поле)
        $worksheet->setCellValue('C50', $chunk['householdServicesRemoteSalesGoods']); // Объектом не оказывались услуги дистанционным способом, в том числе с условием доставки

    }
}
