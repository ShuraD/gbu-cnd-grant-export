<?php

namespace App\ExcelSheetBuilders;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ObjectRightSheetBuilder
{
    public function build(Worksheet $worksheet, $object)
    {
        $worksheet->setCellValue('D4', $object['cadnum']); // Кадастровый номер объекта
        $worksheet->setCellValue('D5', $object['type']); // Тип объекта (здание/помещение)
        $worksheet->setCellValue('D6', $object['square']); // Площадь объекта
        $worksheet->setCellValue('D7', $object['address']); // Адрес объекта
        $worksheet->setCellValue('D8', $object['rightType']); // Вид права претендента на объект
        $worksheet->setCellValue('D9', $object['innOwner']); // Вид права претендента на объект
        $worksheet->setCellValue('D10', $object['nameOwner']); // ИНН собственника объекта (управляющей компании)
        $worksheet->setCellValue('D11', $object['shareRight']); // Доля претендента в праве общей собственности на объект
        $worksheet->setCellValue('D12', $object['documentDetails']); // Наименование и реквизиты правоустанавливающих документов, если право собственности претендента не зарегистрировано в ЕГРН

        $rowShift = 19;
        $worksheet->insertNewRowBefore($rowShift, count($object['chunks']));
        $index = 0;
        $forRents28s = [];
        foreach ($object['chunks'] as $chunk) {
            $worksheet->setCellValue('B' . $rowShift, ++$index); // № п/п
            $worksheet->setCellValue('C' . $rowShift, $chunk['description1']); // Описание части здания/помещения*
            $worksheet->setCellValue('D' . $rowShift, $chunk['name1']); // Наименование объекта (произвольное, например, название магазина)
            $worksheet->setCellValue('E' . $rowShift, $chunk['forRent1']); // Сдается в аренду?
            $worksheet->setCellValue('F' . $rowShift, $chunk['leaseRenterInn1']); // ИНН арендатора
            $worksheet->setCellValue('G' . $rowShift, $chunk['leaseRenterName1']); // Наименование арендатора
            $worksheet->setCellValue('H' . $rowShift, $chunk['leaseDate1']); // Дата договора
            $worksheet->setCellValue('I' . $rowShift, $chunk['leaseNumber1']); // Номер договора
            $worksheet->setCellValue('J' . $rowShift, $chunk['leasePurpose1']); // Цель предоставления площади в аренду по договору
            $worksheet->setCellValue('K' . $rowShift, $chunk['leaseEnded1']); // Расторгнут ли договор до 28.03.2020 (до 01.04.2020 для гостиниц)?
            $worksheet->setCellValue('L' . $rowShift, $chunk['forRent28']); // Заключался ли договор аренды в периоде с 01.03.2020 до 28.03.2020 (до 01.04.2020 для гостиниц)?
//            $worksheet->setCellValue('M' . $rowShift, $chunk['square28']); // Площадь (если сдается в аренду - то из договора аренды)
            $worksheet->setCellValue('N' . $rowShift, $chunk['wasSuspended1']); // Была ли деятельность этого объекта приостановлена согласно указу мэра?
            $worksheet->setCellValue('O' . $rowShift, $chunk['patent1']); // Реквизиты патента арендатора (если есть)
            $worksheet->setCellValue('P' . $rowShift, $chunk['guestCertificate1']); // Свидетельство о присвоении гостинице категории -- Номер
            $worksheet->setCellValue('Q' . $rowShift, $chunk['guestCertificateDate1']); // Свидетельство о присвоении гостинице категории -- Дата
            $rowShift++;
            if ($chunk['forRent28']) {
                $forRents28s[$index] = $chunk;
            }
        }
        $rowShift += 23;
        $worksheet->insertNewRowBefore($rowShift, count($forRents28s));
        $index = 0;
        foreach ($forRents28s as $rowNum => $forRents28) {
            $index++;
            $worksheet->setCellValue('A' . $rowShift, $index); //
            $worksheet->setCellValue('B' . $rowShift, $rowNum); //
            $worksheet->setCellValue('C' . $rowShift, $forRents28['description28']); //
            $worksheet->setCellValue('D' . $rowShift, $forRents28['name1']); //
            $worksheet->setCellValue('E' . $rowShift, $forRents28['forRent1']); //
            $worksheet->setCellValue('F' . $rowShift, $forRents28['leaseRenterInn1']); //
            $worksheet->setCellValue('G' . $rowShift, $forRents28['leaseRenterName1']); //
            $worksheet->setCellValue('H' . $rowShift, $chunk['leaseDate1']); //
            $worksheet->setCellValue('I' . $rowShift, $chunk['leaseNumber1']); //
            $worksheet->setCellValue('J' . $rowShift, $chunk['leasePurpose1']); //
//            $worksheet->setCellValue('K' . $rowShift, $chunk['square28']); //
            $worksheet->setCellValue('L' . $rowShift, $chunk['wasSuspended1']); //
            $worksheet->setCellValue('M' . $rowShift, $chunk['guestCertificate1']); //
            $worksheet->setCellValue('N' . $rowShift, $chunk['guestCertificateDate1']); //

        }
        $rowShift += 15;
        $worksheet->setCellValue('D' . $rowShift, $object['isPpropertyTaxPaid']); // Платится ли налог на имущество в отношении объекта?
        $worksheet->setCellValue('D' . ++$rowShift, $object['taxDeterminingProcedure']); // Порядок определения налоговой базы
        $worksheet->setCellValue('D' . ++$rowShift, $object['taxBase']); // Размер налоговой базы на 01.01.2020
        $worksheet->setCellValue('D' . ++$rowShift, $object['amountTaxBeforeDeductionMonth']); // Сумма налога за март 2020 года до вычета возможных льгот
        $worksheet->setCellValue('D' . ++$rowShift, $object['amountTaxAfterDeductionMonth']); // Сумма налога за март 2020 года после вычета возможных льгот
        $worksheet->setCellValue('D' . ++$rowShift, $object['amountTaxBeforeDeductionQuarter']); // Сумма налога за 2 квартал 2020 года до вычета возможных льгот
        $worksheet->setCellValue('D' . ++$rowShift, $object['amountTaxAfterDeductionQuarter']); // Сумма налога за 2 квартал 2020 года после вычета возможных льгот
    }
}
