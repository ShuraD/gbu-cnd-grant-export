<?php

namespace App\ExcelSheetBuilders;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class LeaseContractSheetBuilder
{
    public function build(Worksheet $worksheet, $chunk, $object, $leaseHolder)
    {
        $worksheet->setCellValue('C4', $object['cadnum']); // Кадастровый номер здания/помещения, в котором расположен объект
        $worksheet->setCellValue('C5', null); // Номер строки в таблице 2 листа "Объект права", в которой указан объект
        $worksheet->setCellValue('C6', null); // Номер строки в таблице 3 листа "Объект права", в которой указан объект
        $worksheet->setCellValue('C7', $leaseHolder['inn']); // ИНН
        $worksheet->setCellValue('C8', $leaseHolder['fullname']); // Наименование арендатора
        $worksheet->setCellValue('C9', $chunk['leaseNumber1']); // Номер договора аренды
        $worksheet->setCellValue('C10', $chunk['leaseDate1']); // Дата договора аренды
        $worksheet->setCellValue('C11', $chunk['leasePurpose1']); // Цель предоставления площади в аренду по договору
        $worksheet->setCellValue('C12', $chunk['leaseStartDate']); // Дата начала аренды
        $worksheet->setCellValue('C13', $chunk['leaseEndDate']); // Дата окончания аренды
        $worksheet->setCellValue('C14', $chunk['isInterdependentPerson']); // Являяется ли арендатор взаимозависимым лицом по отношению к претенденту (по критериям, указанным в ст. 105 НК РФ)?
        // Арендная плата, которая подлежала уплате в соответствии с ранее дейстовавшими условиями договора (до снижения):
        $worksheet->setCellValue('C16', $chunk['leaseRentBefore2003']); // Март 2020 г.
        $worksheet->setCellValue('C17', $chunk['leaseRentBefore2004']); // Апрель 2020 г.
        $worksheet->setCellValue('C18', $chunk['leaseRentBefore2005']); // Май 2020 г.
        $worksheet->setCellValue('C19', $chunk['leaseRentBefore2006']); // Июнь 2020 г.
        $worksheet->setCellValue('C20', $chunk['isRentReduced']); // Была ли снижена арендная плата?
        $worksheet->setCellValue('C21', $chunk['rentalAgreementNumber']); // Номер соглашения о снижении арендной платы
        $worksheet->setCellValue('C22', $chunk['rentalAgreementDate']); // Дата соглашения о снижении арендной платы
        $worksheet->setCellValue('C23', $chunk['rentalReducedSince']); // Дата, с которой снижены арендная плата
        $worksheet->setCellValue('C24', $chunk['rentalReducedBy']); // Дата, по которую снижены арендная плата
        // Совокупная арендная плата после снижения:
        $worksheet->setCellValue('C26', $chunk['leaseRentAfter2003']); // Март 2020 г.
        $worksheet->setCellValue('C27', $chunk['leaseRentAfter2004']); // Апрель 2020 г.
        $worksheet->setCellValue('C28', $chunk['leaseRentAfter2005']); // Май 2020 г.
        $worksheet->setCellValue('C29', $chunk['leaseRentAfter2006']); //  Июнь 2020 г.
        // Заполняется если аренда не была снижена
        $worksheet->setCellValue('C31', $chunk['reduceOfferDate']); // Дата направления оферты
        $worksheet->setCellValue('C32', $chunk['reduceOfferDeliveryType']); // Способ направления оферты
        $worksheet->setCellValue('C33', $chunk['proposedReduceRentDateFrom']); // Дата, с которой предлагалось снизить арендную плату
        $worksheet->setCellValue('C34', $chunk['proposedReduceRentDateTo']); // Дата, по которую предлагалось снизить арендную плату
        // Совокупная арендная плата, которая была бы после предложенного снижения:
        $worksheet->setCellValue('C36', $chunk['leaseRentOffer2003']); // Март 2020 г.
        $worksheet->setCellValue('C37', $chunk['leaseRentOffer2004']); // Апрель 2020 г.
        $worksheet->setCellValue('C38', $chunk['leaseRentOffer2005']); // Май 2020 г.
        $worksheet->setCellValue('C39', $chunk['leaseRentOffer2006']); // Июнь 2020 г.
        $worksheet->setCellValue('C40', $chunk['reduceOfferConfirmation']); // Имеется ли документальное подтверждение факта получения арендатором оферты?
        $worksheet->setCellValue('C41', $chunk['reduceOfferAccepted']); // Была ли оферта акцептована арендатором?
        $worksheet->setCellValue('C42', $chunk['leaseRentFinishInitiator']); // Если договор был прекращен в течение марта - июня 2020 года, это произошло по требованию (по инициативе) арендатора?
    }
}
