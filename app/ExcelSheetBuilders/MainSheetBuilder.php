<?php

namespace App\ExcelSheetBuilders;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class MainSheetBuilder
{
    public function build(Worksheet $worksheet, $source)
    {
        $worksheet->setCellValue('C5', $source['applicantInformation']['inn']); // ИНН
        $worksheet->setCellValue('C6', $source['applicantInformation']['fullname']); // Полное наименование
        $worksheet->setCellValue('C7', $source['applicantInformation']['legalAddress']); // Юридический адрес
        $worksheet->setCellValue('C8', $source['applicantInformation']['postalAddress']); // Почтовый адрес
        $worksheet->setCellValue('C9', $source['applicantInformation']['contacts'][0]['contactPerson'] ?? null); // Контактное лицо 1
        $worksheet->setCellValue('C10', $source['applicantInformation']['contacts'][0]['contactTelephone'] ?? null); // Контактный телефон 1
        $worksheet->setCellValue('C11', $source['applicantInformation']['contacts'][0]['email'] ?? null); // Электронная почта 1
        $worksheet->setCellValue('C12', $source['applicantInformation']['contacts'][1]['contactPerson'] ?? null); // Контактное лицо 2
        $worksheet->setCellValue('C13', $source['applicantInformation']['contacts'][1]['contactTelephone'] ?? null); // Контактный телефон 2
        $worksheet->setCellValue('C14', $source['applicantInformation']['contacts'][1]['email'] ?? null); // Электронная почта 2
        $worksheet->setCellValue('C15', $source['applicantInformation']['contacts'][2]['contactPerson'] ?? null); // Контактное лицо 3
        $worksheet->setCellValue('C16', $source['applicantInformation']['contacts'][2]['contactTelephone'] ?? null); // Контактный телефон 3
        $worksheet->setCellValue('C17', $source['applicantInformation']['contacts'][2]['email'] ?? null); // Электронная почта 3
        $worksheet->setCellValue('C18', $source['applicantInformation']['ceoName']); // ФИО руководителя
        $worksheet->setCellValue('C19', $source['applicantInformation']['chiefAccountantName']); // ФИО главного бухгалтера
        $worksheet->setCellValue('C20', $source['applicantInformation']['isMif'] == '0' ? 'Нет' : 'Да'); // Является ли претендент управляющей компанией закрытого паевого инвестиционного фонда?

        $worksheet->setCellValue('C23', $source['buildingInformation']['cadastralNumber']); // Кадастровый номер здания (сооружения)
        $worksheet->setCellValue('C24', $source['buildingInformation']['address']); // Адрес здания (сооружения)
        $worksheet->setCellValue('C25', $source['buildingInformation']['square']); // Общая площадь здания (сооружения), кв. м
        $worksheet->setCellValue('C26', $source['buildingInformation']['registered']); // Запись о регистрации права собственности претендента (доверительного управления) внесена в ЕГРН в отношении здания (сооружения) или помещения в нем?
        $worksheet->setCellValue('C27', $source['buildingInformation']['exists']); // Если право собственности не зарегистрировано, оно существует на здание (сооружение) или помещение?
        // Objects
        $worksheet->insertNewRowBefore(36, count($source['objects']));
        $rowShift = 36;
        $index = 0;
        foreach ($source['objects'] as $cadNum => $object) {
            $worksheet->setCellValue('A' . $rowShift, '3.' . ++$index);
            $worksheet->setCellValue('B' . $rowShift, $object['name']);
            $worksheet->setCellValue('C' . $rowShift, $object['cadnum']);
            $rowShift++;
        }
        // Remove default rows 34, 35
        $worksheet->removeRow(34, 2);
        // Lands
        $rowShift += 6;
        $worksheet->insertNewRowBefore($rowShift, count($source['lands']));
        $index = 0;
        foreach ($source['lands'] as $cadNum => $land) {
            $worksheet->setCellValue('A' . $rowShift, '4.' . ++$index);
            $worksheet->setCellValue('B' . $rowShift, $land['cadastralNumber']);
            $worksheet->setCellValue('C' . $rowShift, $land['square']);
        }
        // Remove default rows 42, 43
        $worksheet->removeRow($rowShift - $index - 1, 2);
        $rowShift += 3;
        $worksheet->setCellValue('C' . ++$rowShift, $source['requirements']['reqRegMoscow']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['requirements']['reqNotAlien']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['requirements']['reqNotAffiliatedToAlien']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['requirements']['reqNotReorganising']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['requirements']['reqNotSued']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['requirements']['reqNotSelfBuilt']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['requirements']['reqZuCompliesUsage']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['requirements']['reqZuRightsRegisterd']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['requirements']['reqSupplientWasntGrantedBefore']);
        $rowShift += 4;
        $worksheet->setCellValue('D' . ++$rowShift, $source['attachedFiles']['fileNotALien']);
        $worksheet->setCellValue('D' . ++$rowShift, $source['attachedFiles']['fileCeoRights']);
        foreach ($source['attachedFiles']['fileFinRights'] as $fileFinRight) {
//            $worksheet->setCellValue('D' . ++$rowShift, $fileFinRight);
        }
//        foreach ($source['attachedFiles']['filesPatentConfirmation'] as $filesPatentConfirmation) {
//            $worksheet->setCellValue('D' . ++$rowShift, $filesPatentConfirmation);
//        }
        $rowShift += 2;
        $worksheet->setCellValue('D' . ++$rowShift, $source['attachedFiles']['fileNalogCalculation']);
//        foreach ($source['attachedFiles']['filesRights'] as $filesRights) {
//            $worksheet->setCellValue('D' . ++$rowShift, $filesRights);
//        }
//        foreach ($source['attachedFiles']['filesRights'] as $filesAdditionals) {
//            $worksheet->setCellValue('D' . ++$rowShift, $filesAdditionals);
//        }
        $rowShift += 2;
        // 7
        $rowShift += 3;
        $worksheet->setCellValue('A' . ++$rowShift, $source['additionalComments']);
        // 8
        $rowShift += 9;
        $worksheet->setCellValue('C' . ++$rowShift, $source['confirmCheck']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['confirmNotifications']);
        // 9
        $rowShift += 3;
        $worksheet->setCellValue('C' . ++$rowShift, $source['signerCode']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['signerMane']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['signerInn']);
        $worksheet->setCellValue('C' . ++$rowShift, $source['signerCOnfirmationDoc']);

    }
}
