<?php

namespace App\ExcelSheetBuilders;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class LandSheetBuilder
{
    public function build(Worksheet $worksheet, $land)
    {
        $worksheet->setCellValue('C6', $land['cadastralNumber']); // Кадастровый номер участка
        $worksheet->setCellValue('C7', $land['square']); // Площадь участка
        $worksheet->setCellValue('C8', $land['address']); // Адрес участка
        $worksheet->setCellValue('C9', $land['rightType']); // На каком праве используется участок?
        $worksheet->setCellValue('C10', $land['ownerInn']); // ИНН собственника участка
        $worksheet->setCellValue('C11', $land['ownerName']); // Наименование собственника участка
        $worksheet->setCellValue('C12', $land['shareRight']); // Доля претендента в праве общей собственности на земельный участок
        $worksheet->setCellValue('C13', $land['ownRightsDocument']); // Если участок в собственности, но сведений об этом нет в ЕГРН - информация об основаниях возникновения права собственности (в т.ч. реквизиты правоустанавливающего документа)
        $worksheet->setCellValue('C14', $land['ownRightsExplanation']); // Пояснения о причинах того, что права претендента на использование земельного участка не оформлены
        $worksheet->setCellValue('C15', $land['comments']); // Комментарий по виду прав претендента на земельный участок, если указано "иное"

        $worksheet->setCellValue('C17', $land['ownTaxPaid']); // Платится ли земельный налог в отношении объекта?
        $worksheet->setCellValue('C18', $land['ownTaxBaseSum']); // Размер налоговой базы на 01.01.2020
        $worksheet->setCellValue('C19', $land['ownTaxSum']); // Ставка налога
        $worksheet->setCellValue('C20', $land['ownTaxSum2003before']); // Сумма налога за март 2020 года до вычета возможных льгот
        $worksheet->setCellValue('C21', $land['ownTaxSum2003after']); // Сумма налога за март 2020 года после вычета возможных льгот
        $worksheet->setCellValue('C22', $land['ownTaxSumQuartbefore']); // Сумма налога за 2 квартал 2020 года до вычета возможных льгот
        $worksheet->setCellValue('C23', $land['ownTaxSumQuartafter']); // Сумма налога за 2 квартал 2020 года после вычета возможных льгот

        $worksheet->setCellValue('C25', $land['rentIsPaid']); // Платится ли арендная плата в отношении объекта?
        $worksheet->setCellValue('C26', $land['rentSum2003']); // Размер арендной платы за март 2020 года
        $worksheet->setCellValue('C27', $land['rentSumQuart']); // Размер арендной платы за 2 квартал 2020 года
        $worksheet->setCellValue('C28', $land['rentDate']); // Дата договора аренды земельного участка
        $worksheet->setCellValue('C29', $land['rentNum']); // Номер договора аренды земельного участка
        // TODO: cycle
        $worksheet->insertNewRowBefore(35, count($land['zuokss']));
        foreach ($land['zuokss'] as $index => $zuokss) {
            $worksheet->setCellValue('B' . (35 + $index), $zuokss); // Перечень объектов недвижимости, расположенных на участке
        }
    }
}
