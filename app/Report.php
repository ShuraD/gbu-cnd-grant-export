<?php

namespace App;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\ExcelSheetBuilders\MainSheetBuilder;
use App\ExcelSheetBuilders\ObjectRightSheetBuilder;
use App\ExcelSheetBuilders\LeaseContractSheetBuilder;
use App\ExcelSheetBuilders\LandSheetBuilder;
use App\ExcelSheetBuilders\SuspenseSheetBuilder;

class Report
{
    const EXPORT_TEMPLATE = 'export_v6_template.xlsx';

    public function build($source, $filename)
    {
        $spreadsheet = IOFactory::load(__DIR__ . DIRECTORY_SEPARATOR . self::EXPORT_TEMPLATE);
        $mainSheet = $spreadsheet->getSheetByName('Основной лист заявки');
        $mainSheetBuilder = new MainSheetBuilder();
        $mainSheetBuilder->build($mainSheet, $source);
        // Доп лист Объект права
        // Доп лист Договор аренды
        // Доп лист Приостановка
        $objectRightSheetBuilder = new ObjectRightSheetBuilder();
        $leaseContractSheetBuilder = new LeaseContractSheetBuilder();
        $suspenseSheetBuilder = new SuspenseSheetBuilder();
        $baseObjectRightSheet = $spreadsheet->getSheetByName('Доп. лист Объект права');
        $baseLeaseContractSheet = $spreadsheet->getSheetByName('Доп. лист Договор аренды');
        $baseSuspenseSheet = $spreadsheet->getSheetByName('Доп. лист Приостановка');
        $index = 0;
        foreach ($source['objects'] as $object) {
            $index++;
            $objectRightSheet = clone $baseObjectRightSheet;
            $objectRightSheet->setTitle('Доп. лист Объект права ' . $index);
            $objectRightSheetBuilder->build($objectRightSheet, $object);
            $spreadsheet->addSheet($objectRightSheet);
            $chunkLCIndex = 0;
            $chunkSIndex = 0;
            foreach ($object['chunks'] as $chunk) {
                if ($chunk['forRent1']) {
                    $chunkLCIndex++;
                    $leaseContractSheet = clone $baseLeaseContractSheet;
                    $leaseContractSheetBuilder->build($leaseContractSheet, $chunk, $object, $source['applicantInformation']);
                    $leaseContractSheet->setTitle('Доп. лист Договор аренды ' . $chunkLCIndex);
                    $spreadsheet->addSheet($leaseContractSheet);
                }
                if ($chunk['wasSuspended1']) {
                    $chunkSIndex++;
                    $susprenseSheet = clone $baseSuspenseSheet;
                    $suspenseSheetBuilder->build($susprenseSheet, $chunk, $object, $source['applicantInformation']);
                    $susprenseSheet->setTitle('Доп. лист Приостановка ' . $chunkSIndex);
                    $spreadsheet->addSheet($susprenseSheet);
                }
            }
        }
        // Доп лист Участок
        $landSheetBuilder = new LandSheetBuilder();
        $index = 0;
        $baseLandSheet = $spreadsheet->getSheetByName('Доп. лист Участок');
        foreach ($source['lands'] as $land) {
            $landSheet = clone $baseLandSheet;
            $landSheetBuilder->build($landSheet, $land);
            $landSheet->setTitle('Доп. лист Участок ' . ++$index);
            $spreadsheet->addSheet($landSheet);
        }
        // Remove default sheets
        $spreadsheet->removeSheetByIndex($spreadsheet->getIndex($baseObjectRightSheet));
        $spreadsheet->removeSheetByIndex($spreadsheet->getIndex($baseLeaseContractSheet));
        $spreadsheet->removeSheetByIndex($spreadsheet->getIndex($baseSuspenseSheet));
        $spreadsheet->removeSheetByIndex($spreadsheet->getIndex($baseLandSheet));

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . DIRECTORY_SEPARATOR . $filename);
    }
}
