# Lumen PHP Framework

## Deployment

```bash
composer install
```

## Usage
```
POST /export HTTP/1.1
Content-Type: application/json

{
    "applicantInformation": {
        "inn": "5003054733",
        "fullname": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"СВЯЗЬЭЛЕКТРОСЕРВИС\"",
        "legalAddress": "null null, УЧАСТОК 190Ю/1 КОНТОРА ПРАВЛЕНИЯ ",
        "postalAddress": "Почтовый адрес",
        "contacts": [
            {
                "contactPerson": "Контактное лицо 1",
                "contactTelephone": "+7 (000) 000-00-00",
                "email": "email@contact.com"
            }
        ],
        "ceoName": "ФИО руководителя",
        "chiefAccountantName": "ФИО главного бугалтера",
        "isMif": false
    },
    "buildingInformation": {
        "cadastralNumber": "77:17:0000000:6595",
        "address": "г. Москва, п. Сосенское, д. Сосенки, Дом 13Г ",
        "square": 543.4,
        "registered": "Здание",
        "exists": null
    }, 
    "objects": {
        "77:17:0000000:6595": {
            "name": "",
            "cadnum": "77:17:0000000:6595",
            "type": "Здание",
            "square": 543.4,
            "address": "г. Москва, п. Сосенское, д. Сосенки, Дом 13Г ",
            "hasRights": true,
            "rightType": "own",
            "innOwner": "5003054733",
            "nameOwner": "Общество с ограниченной ответственностью \" СвязьЭлектроСервис\"",
            "shareRight": "1",
            "documentDetails": null,
            "isPpropertyTaxPaid": true,
            "taxDeterminingProcedure": "от кадастровой стоимости",
            "taxBase": "999999",
            "amountTaxBeforeDeductionMonth": "9999",
            "amountTaxAfterDeductionMonth": "9999",
            "amountTaxBeforeDeductionQuarter": "9999",
            "amountTaxAfterDeductionQuarter": "9999",
            "chunks": [
                {
                    "description1": "Гостиница",
                    "name1": "dsafga",
                    "square1": null,
                    "forRent1": true,
                    "leaseRenterInn1": null,
                    "leaseRenterName1": null,
                    "leaseDate1": null,
                    "leaseNumber1": null,
                    "leasePurpose1": null,
                    "leaseEnded1": null,
                    "wasSuspended1": true,
                    "patent1": null,
                    "guestCertificate1": null,
                    "guestCertificateDate1": null,
                    "description28": null,
                    "forRent28": true,
                    "leaseRenterInn28": null,
                    "leaseRenterName28": null,
                    "leaseDate28": null,
                    "leaseNumber28": null,
                    "leasePurpose28": null,
                    "wasSuspended28": null,
                    "patent28": null,
                    "guestCertificate28": null,
                    "guestCertificateDate28": null,
                    "leaseStartDate": null,
                    "leaseEndDate": null,
                    "isInterdependentPerson": null,
                    "leaseRentBefore2003": null,
                    "leaseRentBefore2004": null,
                    "leaseRentBefore2005": null,
                    "leaseRentBefore2006": null,
                    "isRentReduced": null,
                    "rentalAgreementNumber": null,
                    "rentalAgreementDate": null,
                    "rentalReducedSince": null,
                    "rentalReducedBy": null,
                    "leaseRentAfter2003": null,
                    "leaseRentAfter2004": null,
                    "leaseRentAfter2005": null,
                    "leaseRentAfter2006": null,
                    "reduceOfferDate": null,
                    "reduceOfferDeliveryType": null,
                    "proposedReduceRentDateFrom": null,
                    "proposedReduceRentDateTo": null,
                    "leaseRentOffer2003": null,
                    "leaseRentOffer2004": null,
                    "leaseRentOffer2005": null,
                    "leaseRentOffer2006": null,
                    "reduceOfferConfirmation": null,
                    "reduceOfferAccepted": null,
                    "leaseRentFinishInitiator": null,
                    "suspensionDate": null,
                    "suspensionEnd": null,
                    "cateringFacilityType": null,
                    "servicedByTakeaway": null,
                    "workedDeliveryOrders": null,
                    "cateringEmployeeOrganization": null,
                    "retailEntity": null,
                    "sfNOTpharmacy": null,
                    "sfNOTpharmacyPoint": null,
                    "sfNOTcommunicationServicesProvision": null,
                    "sfNOToptophthalmologicalProvision": null,
                    "sfNOTpetSupplesProvision": null,
                    "sfNOTfoodProvision": null,
                    "sfNOTfirstFoodsProvision": null,
                    "shoppingFacilityRemoteSaleGoods": null,
                    "householdServicesType": null,
                    "householdServicesComment": null,
                    "householdServicesRemoteSalesGoods": null
                }
            ],
            "chunkIds": []
        }
    },
    "lands": {
        "50:21:0120106:825": {
            "cadastralNumber": "50:21:0120106:825",
            "square": 2176,
            "address": "142791, Москва, Сосенское поселение, деревня Сосенки, Участок Владение 13Г",
            "hasRights": true,
            "rightType": "rentMsk",
            "ownerInn": "5003054733",
            "ownerName": "Общество с ограниченной ответственностью \" СвязьЭлектроСервис\"",
            "shareRight": "1",
            "ownRightsDocument": null,
            "ownRightsExplanation": null,
            "comments": null,
            "ownTaxPaid": null,
            "ownTaxBaseSum": null,
            "ownTaxSum": null,
            "ownTaxSum2003before": null,
            "ownTaxSum2003after": null,
            "ownTaxSumQuartbefore": null,
            "ownTaxSumQuartafter": null,
            "rentIsPaid": true,
            "rentSum2003": "9999",
            "rentSumQuart": "9999",
            "rentDate": "2020-05-24T21:00:00.000Z",
            "rentNum": "00000",
            "zuokss": [
                "77:17:0000000:6595",
                "77:17:0120106:179"
            ],
            "zuokssSelf": [],
            "_showDetails": true
        }
    },
    "requirements": {
        "reqRegMoscow": true,
        "reqNotAlien": true,
        "reqNotAffiliatedToAlien": true,
        "reqNotReorganising": true,
        "reqNotSued": true,
        "reqNotSelfBuilt": true,
        "reqZuCompliesUsage": true,
        "reqZuRightsRegisterd": true,
        "reqSupplientWasntGrantedBefore": true
    },
    "attachedFiles": {
        "fileNotALien": "fileNotALien",
        "fileCeoRights": "fileCeoRights",
        "fileFinRights": [
            "fileFinRights1",
            "fileFinRights2"
        ],
        "filesPatentConfirmation": [
            "filesPatentConfirmation1",
            "filesPatentConfirmation2"
        ],
        "fileNalogCalculation": "fileNalogCalculation",
        "filesRights": [
            "filesRights1",
            "filesRights2"
        ],
        "filesAdditionals": [
            "filesAdditionals1",
            "filesAdditionals2"
        ]
    },
    "additionalComments": "Ваш комментарий",
    "confirmCheck": true,
    "confirmNotifications": true,
    "signerCode": "signerCode",
    "signerMane": "signerMane",
    "signerInn": "signerInn",
    "signerCOnfirmationDoc": "signerCOnfirmationDoc",
    "settings": {
        "collapse": {}
    }
}
```
